# answers to the questions

## Qn 1
1. Regarding on how to update the User there are two options and below are the following :-
 a). Non - customized User.

```
user = User.objects.get(pk=x)
user.firstname = "xxx"
user.save()

```
this way above we can be able to save the user by searching with the promary key (pk)

b). customized User.

for the customized user we use a function called `get_user_model()` meaning the snippet below is how to 
to update the user : 

```
user = get_user_model().objects.get(pk=x)
user.firstname = "xxx"
user.save()
```

So, in summary, if you want to customize more of your authentication functionality use `get_user_model()` if not then use the default `User` class

## Qn 2

2. If am to get the name of the groups a user belongs to in a list , first I will first query or get the the user :

```
user = get_user_model().objects.get(pk=x)
```

then get the names of the groups for the user we with the help of the `values()` function to filter the queryset by the name of the group.

Below is the queryset :

```
grps = user.groups.all().values("name")
```




## Qn 3.

3. Regarding getting the count (integer) of users that signed up between 2016-01-01 and 2016-04-01,

we have to use the filter object which will give room to query between two give data points for our case they are dates, so a queryset will 
be generated then after
that we add a count function to count all of them, thus returning an integer. 

So, what we can literary do is :

```
users_date_joined = User.objects.filter(date_joined__range=["2016-01-01","2016-04-01"]).count()

```
or 

```
users_date_joined = get_user_model().objects.filter(date_joined__range=["2016-01-01","2016-04-01"]).count()
```